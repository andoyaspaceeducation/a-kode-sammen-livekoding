# Kodeoppdrag 2: Angrep fra rommet - et spill?
# 
# Det er et romskip over skolen og vi må samles i biblioteket for å legge en forsvarsplan.
# 
from time import sleep

tid_til_angrep = 10
hvem_er_med = []
ditt_navn = ""

print("Det har blitt observert et romskip over skolen. Vi må samles i biblioteket for å legge en plan.\n")

ditt_navn = input("Hva heter du? ")
hvem_er_med = input("Hvem andre er med? ").replace(" ", "").split(",")
hvem_er_med.append(ditt_navn)
print("\nFlott, da har vi med oss {} stykker, kjempebra {}!\n".format(len(hvem_er_med), ditt_navn.capitalize()))

for navn in sorted(hvem_er_med):
    print(navn.capitalize())

print("\nRomvesene angriper om {} minutter\n".format(tid_til_angrep))

tid_til_biblioteket = int(input("Hvor mange minutter tar det å komme seg til biblioteket? "))

tid = tid_til_angrep-tid_til_biblioteket

if tid >= 5:
    print("\nDa har vi {} minutter på oss, ingen tid å miste!\n".format(tid))
    for minutt in range(1,tid+1,1):
        sleep(1)
        print("Det har nå gått {} minutt".format(minutt))
        
elif tid > 0 < 5:
    print("\nDa har vi bare {} minutter på oss. Vi må virkelig skynde oss!\n".format(tid))
    for minutt in range(1,tid+1,1):
        sleep(1)
        print("Det har nå gått {} minutt".format(minutt))

elif tid < 1:
    print("\nDa har vi ingen sjanse, vi rekker det ikke. Lykke til!".format(tid))

print("\nFremme ved biblioteket. Da kan vi begynne planleggingen vår. Vi må legge en strategi for hvordan vi skal beskytte skolen vår.\n")
med_eller_ikke = input("Er du med {}? (Svar ja eller nei) ".format(ditt_navn.capitalize()))

if med_eller_ikke.lower() == "ja":
    print("\nVeldig bra {}, da setter vi i gang, takk for denne gangen...\n".format(ditt_navn.capitalize()))
if med_eller_ikke.lower() == "nei":
    print("\nDet er forståelig {}, takk for denne gangen...og lykke til!\n".format(ditt_navn.capitalize()))

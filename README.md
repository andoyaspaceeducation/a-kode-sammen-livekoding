# Å kode sammen – livekoding
Disse kildekodefilene er en del av en aktivitet for lærere på esero.no

## Kort om aktiviteten
Programmering og algoritmisk tenkning er synliggjort i fagene på ulikt vis i Læreplanverket for Kunnskapsløftet 2020. Elevene får en introduksjon til sentrale konsepter og grunnstrukturer i programmering i matematikkfaget, men de skal lære og anvende kompetansen også i andre fag. Det er eksplisitte kompetansemål i naturfag, kunst og håndverk og musikk. Programmering kan også styrke andre fag, som for eksempel samfunnsfag og norsk.
Mange lærere er usikre på hvordan de skal undervise i programmering og algoritmisk tenkning i sine fag. Denne øvelsen forsøker å vise programmering gjennom at læreren tar i bruk kodebegreper som funksjoner, løkker og vilkår på direkten, sammen med elevene. 

## Læringsmål
* Få en bedre forståelse for de grunnleggende begrepene i programmering 
* Få bedre kjennskap til Python som programmeringsspråk
* Bli kjent med livekoding som metode for å lære programmering


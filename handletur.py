# Kodeoppdrag 1: En handletur på butikken
# 
# Hva må tenkes på for å gjennomføre en handletur?
# 
from time import sleep
 
def handletur():
    steg = 0
    while steg < avstand:
        print("Gått", steg, "steg")
        steg = steg + 1
        sleep(0.5)
    print("\nFremme ved butikken\n")
    for vare in sorted(handleliste):
        if vare not in svarteliste:
            print("plukke opp varen", vare)
            sleep(0.5)
 
handle = input("Skal du handle, Ja eller Nei ")
handleliste = ["mel", "egg", "sukker", "øl", "middag"]
svarteliste = ["pizza", "øl"]
 
if handle == "Ja":
    avstand = int(input("Hvor langt er det til butikken? "))
    print("Gå til butikken")
    handletur()
    print("\nFerdig å handle, gå hjem igjen")
else:
    print("Bli hjemme på sofaen")
